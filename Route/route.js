const books = require('../models/book');
module.exports = (app)=>{
    app.post("/books", async (request, response) => {
        try {
            var person = new books(request.body);
            var result = await person.save();
            response.send(result);
        } catch (error) {
            response.status(500).send(error);
        }
    });
    app.get("/books", async (request, response) => {
        try {
            var result = await books.find().exec();
            response.send(result);
        } catch (error) {
            response.status(500).send(error);
        }
     });
    app.get("/books/:id", async (request, response) => {
        try {
            var person = await books.findById(request.params.id).exec();
            response.send(person);
        } catch (error) {
            response.status(500).send(error);
        }
    });
    app.put("/books/:id", async (request, response) => {
        try {
            var person = await books.findById(request.params.id).exec();
            person.set(request.body);
            var result = await person.save();
            response.send(result);
        } catch (error) {
            response.status(500).send(error);
        }
    });
    app.delete("/books/:id", async (request, response) => {
        try {
            var result = await books.deleteOne({ _id: request.params.id }
            ).exec();
            response.send(result);
        } catch (error) {
            response.status(500).send(error);
        }
    });

    app.use(function(req, res, next){
        return res.status(404).send({
            status: 404,
            message: "not Found"
        });
    });

    app.use(function(err,req,res,next){
        return res.status(505).send(err);
    });
};
