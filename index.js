const Express = require("express");
const BodyParser = require("body-parser");

const app = Express();
const PORT = 3000;
const DOMAIN = "localhost";

app.use(BodyParser.json());
app.use(BodyParser.urlencoded({ extended: true }));

require('./Route/route.js')(app);

app.listen(PORT,DOMAIN, () => {
    console.log(`Server Is listening on https://${DOMAIN}:${PORT}`);
});