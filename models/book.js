const Mongoose = require("mongoose");

Mongoose.connect("mongodb://localhost:27017/nodedb",{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const books = Mongoose.model("book", {
    title:  {
        type: String,
        minlength: [5, 'minimal 5 karakter'],
        unique: true
      },
    author: [
        {
            type: String,
            minlength: [5, 'minimal 5 karakter'],
            unique: true
        },
    ],
    published_date : {
            type: Date,
            default: Date.now
        },
    pages:  {
        type: Number,
        min: [5, 'minimal 5 karakter']   
    },
    language:  {
        type: String,
        minlength: [5, 'minimal 5 karakter']  
    },
    publisher_id:  {
        type: String,
        minlength: [5, 'minimal 5 karakter']   
    }, 
});

module.exports = books